//====================================================================================================

function circle_points(radius, nb) {
	const delta = 360 / nb;

	let points = [];
	for (let i = 0; i < nb; i++) {
		let x = cos(delta * i) * radius;
		let y = sin(delta * i) * radius;
		let point = {
			x: x,
			y: y
		};
		points.push(point);
	}
	points.push({
		x: radius,
		y: 0
	})
	return points;
}

function mk_cylinder(radius, height, precision) {
	const delta = 2.0 * acos(1.0 - (precision / radius));
	const nb = round(360 / delta);
	const points = circle_points(radius, nb);

	let polygons = [];

	for (let i = 0; i < points.length - 1; i++) {
		let p1 = points[i];
		let p2 = points[i + 1];
		let v1 = new CSG.Vertex(new CSG.Vector3D(p1.x, p1.y, 0));
		let v2 = new CSG.Vertex(new CSG.Vector3D(p2.x, p2.y, 0));
		let v3 = new CSG.Vertex(new CSG.Vector3D(p2.x, p2.y, height));
		let v4 = new CSG.Vertex(new CSG.Vector3D(p1.x, p1.y, height));
		let face = new CSG.Polygon([v1, v2, v3, v4]);
		polygons.push(face);
	}

	let top = [];
	let bot = [];
	for (let i = 0; i < points.length - 1; i++) {
		let p = points[i];
		top.push(new CSG.Vertex(new CSG.Vector3D(p.x, p.y, height)));
		bot.unshift(new CSG.Vertex(new CSG.Vector3D(p.x, p.y, 0)));
	}
	polygons.push(new CSG.Polygon(top));
	polygons.push(new CSG.Polygon(bot));

	let solid = CSG.fromPolygons(polygons);
	return solid;
}

function mk_tube(radius, height, thickness, precision) {
	let ext = mk_cylinder(radius, height, precision);
	let int = mk_cylinder(radius - thickness, height, precision);
	return difference(ext, int);
}

function mk_elliptical_cone(radius, height, precision) {

	function R(z) {
		return radius / height * sqrt(pow(height, 2) - pow(z, 2));
	}

	const delta = 2.0 * acos(1.0 - (precision / radius));
	const nb = round(360 / delta);

	let z_list = [];
	z_list.push({
		z: 0,
		r: radius
	});
	z_list.push({
		z: height,
		r: 0
	});

	for (let i = 0; i < z_list.length - 1; i++) {
		let z1 = z_list[i];
		let z2 = z_list[i + 1];
		let zm = (z1.z + z2.z) / 2;
		let rm = R(zm);
		let err = abs(rm - (z1.r + z2.r) / 2);
		if (err > precision) {
			z_list.splice(i + 1, 0, {
				z: zm,
				r: rm
			});
			i--;
		}
	}

	for (let i = 0; i < z_list.length - 1; i++) {
		let z = z_list[i].z
		let r = z_list[i].r
		z_list[i].points = circle_points(r, nb);
	}

	// External faces (tetrahedrons)
	let polygons = [];
	for (let i = 0; i < z_list.length - 2; i++) {
		let lo = z_list[i];
		let up = z_list[i + 1];
		for (let j = 0; j < lo.points.length - 1; j++) {
			let p1 = lo.points[j];
			let p2 = lo.points[j + 1];
			let p3 = up.points[j];
			let p4 = up.points[j + 1];
			let v1 = new CSG.Vertex(new CSG.Vector3D(p1.x, p1.y, lo.z));
			let v2 = new CSG.Vertex(new CSG.Vector3D(p2.x, p2.y, lo.z));
			let v3 = new CSG.Vertex(new CSG.Vector3D(p4.x, p4.y, up.z));
			let v4 = new CSG.Vertex(new CSG.Vector3D(p3.x, p3.y, up.z));
			let face = new CSG.Polygon([v1, v2, v3, v4]);
			polygons.push(face);
		}
	}
	// Last triangles
	let lo = z_list[z_list.length - 2];
	for (let j = 0; j < lo.points.length - 1; j++) {
		let p1 = lo.points[j];
		let p2 = lo.points[j + 1];
		let v1 = new CSG.Vertex(new CSG.Vector3D(p1.x, p1.y, lo.z));
		let v2 = new CSG.Vertex(new CSG.Vector3D(p2.x, p2.y, lo.z));
		let v3 = new CSG.Vertex(new CSG.Vector3D(0, 0, height));
		let face = new CSG.Polygon([v1, v2, v3]);
		polygons.push(face);
	}
	// Bottom
	let bot = [];
	let points = z_list[0].points;
	for (let i = 0; i < points.length - 1; i++) {
		let p = points[i];
		bot.unshift(new CSG.Vertex(new CSG.Vector3D(p.x, p.y, 0)));
	}
	polygons.push(new CSG.Polygon(bot));

	let solid = CSG.fromPolygons(polygons);
	return solid;
}

function mk_nose(radius, height, thickness, precision) {
	let ext = mk_elliptical_cone(radius, height, precision);
	let int = mk_elliptical_cone(radius - thickness, height - thickness, precision);
	return difference(ext, int);
}

//====================================================================================================

function Naca4(profile, c) {
	this.d1 = ceil(Number(profile) / 1000);
	this.d2 = ceil(Number(profile) / 100) % 10;
	this.d34 = Number(profile) % 100;
	this.c = Number(c);

	// 1: cambrure max en % pourcentage de la corde
	this.m = function() {
		return this.d1 / 100;
	}
	// 2: position de la cambrure max en dixieme de la corde
	this.p = function() {
		return this.d2 / 10;
	}
	// 3-4: epaisseur max en pourcentage de la corde
	this.t = function() {
		return this.d34 / 100;
	}

	this.Zt = function(y) {
		return this.t() / 0.2 * this.c * (0.2969 * Math.sqrt(y / this.c) - 0.1260 * (y / this.c) - 0.3516 * Math.pow(y / this.c, 2) + 0.2843 * Math.pow(y / this.c, 3) - 0.1015 * Math.pow(y / this.c, 4));
	}

	this.Zc = function(y) {
		if ((0 <= y) && (y <= this.p() * this.c)) {
			return this.m() * y / Math.pow(this.p(), 2) * (2 * this.p() - y / this.c);
		} else {
			return this.m() * (this.c - y) / Math.pow(1 - this.p(), 2) * (1 + y / this.c - 2 * this.p());
		}
	}

	this.teta = function(y) {
		return Math.atan(this.Zc(y) / y);
	}

	this.yU = function(y) {
		if (y > 0)
			return y - this.Zt(y) * Math.sin(this.teta(y));
		else
			return 0;
	}

	this.yL = function(y) {
		if (y > 0)
			return y + this.Zt(y) * Math.sin(this.teta(y));
		else
			return 0;
	}

	this.zU = function(y) {
		if (y > 0)
			return this.Zc(y) + this.Zt(y) * Math.cos(this.teta(y));
		else
			return 0;
	}

	this.zL = function(y) {
		if (y > 0)
			return this.Zc(y) - this.Zt(y) * Math.cos(this.teta(y));
		else
			return 0;
	}

	this.Y = function(y) {
		return (this.yU(y) + this.yL(y)) / 2;
	}

	this.Z = function(y) {
		return (this.zU(y) + this.zL(y)) / 2;
	}

	this.height = function(y) {
		return (this.zU(y) - this.zL(y)) / 2;
	}
}

function naca_y_list(naca, precision, nb_init = 10) {
	function error(y1, y2, z1, z2, ym, zm) {
		let y = (y1 + y2) / 2
		let z = (z1 + z2) / 2
		return Math.sqrt(Math.pow(y - ym, 2) + Math.pow(z - zm, 2));
	}

	let list = [];
	for (let i = 0; i < (nb_init - 1); i++) {
		list.push(i * naca.c / (nb_init - 1));
	}
	list.push(naca.c);

	for (let i = 0; i < list.length - 1; i++) {
		let y1 = list[i];
		let y2 = list[i + 1];
		let ym = (y1 + y2) / 2;
		// Up
		let yU1 = naca.yU(y1);
		let yU2 = naca.yU(y2);
		let yUm = naca.yU(ym);
		let zU1 = naca.zU(y1);
		let zU2 = naca.zU(y2);
		let zUm = naca.zU(ym);
		let errU = error(yU1, yU2, zU1, zU2, yUm, zUm);
		let yL1 = naca.yL(y1);
		let yL2 = naca.yL(y2);
		let yLm = naca.yL(ym);
		let zL1 = naca.zL(y1);
		let zL2 = naca.zL(y2);
		let zLm = naca.zL(ym);
		let errL = error(yL1, yL2, zL1, zL2, yLm, zLm);
		// console.log("=> " + i);
		// console.log("> error " + errU + " " + errL);
		// console.log("> y " + y1 + " " + y2 + " " + ym);
		if (errU > precision || errL > precision) {
			list.splice(i + 1, 0, ym);
			i--;
		}
	}

	for (let i = 0; i < list.length; i++) {
		list[i] = list[i] / naca.c;
	}

	return list;
}

function loft(bot, top) {
	let polygons = [bot];

	for (let i = 0; i < bot.vertices.length - 1; i++) {
		let p1 = bot.vertices[i];
		let p2 = top.vertices[i];
		let p3 = top.vertices[i + 1];
		let p4 = bot.vertices[i + 1];
		let points = [p1, p2, p3, p4];
		polygons.push(new CSG.Polygon(points));
	}
	let p1 = bot.vertices[bot.vertices.length - 1];
	let p2 = top.vertices[bot.vertices.length - 1];
	let p3 = top.vertices[0];
	let p4 = bot.vertices[0];
	let points = [p1, p2, p3, p4];
	polygons.push(new CSG.Polygon(points));

	polygons.push(new CSG.Polygon(top.vertices.reverse()));

	return CSG.fromPolygons(polygons);
}

function naca_wire(naca, y_list, precision) {
	let points = [];
	// Upper
	for (let i = 0; i < y_list.length; i++) {
		let dy = y_list[i] * naca.c;
		let y = naca.yU(dy);
		let z = naca.zU(dy);
		points.push(new CSG.Vertex(new CSG.Vector3D(0, y, z)));
	}
	// Lower
	for (let i = y_list.length - 1; i > 0; i--) {
		let dy = y_list[i] * naca.c;
		let y = naca.yL(dy);
		let z = naca.zL(dy);
		points.push(new CSG.Vertex(new CSG.Vector3D(0, y, z)));
	}
	return new CSG.Polygon(points);
}

function naca_wing(naca_left, naca_right, width, arrow, precision) {
	let y_list_left = naca_y_list(naca_left, precision);
	let y_list_right = naca_y_list(naca_right, precision);
	let y_list = y_list_left;
	if (y_list_right.length > y_list_left.length)
		y_list = y_list_right;

	let y = tan(arrow) * width;
	let left = naca_wire(naca_left, y_list, precision);
	let right = naca_wire(naca_right, y_list, precision);
	right = right.translate([width, y, 0]);

	return loft(left, right);
}

//====================================================================================================

function getParameterDefinitions() {
	return [
		// Show
		// {
		// 	name: 'Show',
		// 	type: 'choice',
		// 	values: ["ALL", "NOSE", "BODY", "WINGS"],
		// 	captions: ["All", "Nose", "Body", "Wings"],
		// 	initial: "ALL",
		// },
		// Nose Parameters
		{
			name: 'display_nose',
			caption: 'Display Nose',
			type: 'checkbox',
			checked: false,
		}, {
			name: 'nose_length',
			type: 'float',
			initial: 15,
			caption: "Nose length:"
		},
		// Body Parameters
		{
			name: 'display_body',
			caption: 'Display Body',
			type: 'checkbox',
			checked: false,
		}, {
			name: 'body_radius',
			type: 'float',
			initial: 5,
			caption: "Body radius:"
		}, {
			name: 'body_length',
			type: 'float',
			initial: 70,
			caption: "Body length:"
		},
		// Wing Parameters
		{
			name: 'display_wings',
			caption: 'Display Wings',
			type: 'checkbox',
			checked: true,
		}, {
			name: 'wing_naca',
			type: 'text',
			initial: 3410,
			caption: "Wing Naca:"
		}, {
			name: 'wing_width',
			type: 'float',
			initial: 60,
			caption: "Wing width:"
		}, {
			name: 'wing_c_max',
			type: 'float',
			initial: 20,
			caption: "Wing max chord:"
		}, {
			name: 'wing_c_min',
			type: 'float',
			initial: 10,
			caption: "Wing min chord:"
		}, {
			name: 'wing_arrow',
			type: 'float',
			initial: 15,
			caption: "Wing arrow (degree):"
		}, {
			name: 'wing_dihedron',
			type: 'float',
			initial: 5,
			caption: "Wing dihedron (degree):"
		}, {
			name: 'wing_angle',
			type: 'float',
			initial: 3,
			caption: "Wing angle (degree):"
		}, {
			name: 'wing_x',
			type: 'float',
			initial: 1,
			caption: "Wing X (+body):"
		}, {
			name: 'wing_y',
			type: 'float',
			initial: 5,
			caption: "Wing Y (+nose):"
		}, {
			name: 'wing_z',
			type: 'float',
			initial: 2,
			caption: "Wing Z (+body center):"
		},
		// Global Parameters, 
		{
			name: 'precision',
			type: 'float',
			initial: 0.1,
			caption: "Precision:",
			min: 0.001,
			max: 1,
			step: 0.01
		}
	];
}

function main(params) {
	console.log("-------------------------");
	console.log(CSG.defaultResolution3D);
	CSG.defaultResolution3D = 50;
	console.log(CSG.defaultResolution3D);

	let y_min = -50;
	let z_min = params.body_radius;

	let nose_thickness = 0.1;
	let body_thickness = 0.1;

	let result = [];

	// Nose
	if (params.display_nose) {
		let nose = mk_nose(params.body_radius, params.nose_length, nose_thickness, params.precision);
		nose = translate([0, 0, -params.nose_length], nose);
		nose = rotate(90, [1, 0, 0], nose);
		nose = translate([0, y_min, z_min], nose);
		nose = color([1, 0.5, 0], nose);
		result.push(nose);
	}

	// Body
	if (params.display_body) {
		let body = mk_tube(params.body_radius, params.body_length, body_thickness, params.precision);
		body = rotate(-90, [1, 0, 0], body);
		body = translate([0, y_min + params.nose_length, z_min], body);
		body = color([1, 0, 0], body);
		result.push(body);
	}


	// Wing
	if (params.display_wings) {
		// Wing Axe
		let ratio = 0.25;
		let axe_radius = 0.5;
		let axe_y = params.wing_c_max * ratio;
		let y = params.wing_width * tan(params.wing_arrow);
		let dy = y + ratio * (params.wing_c_min - params.wing_c_max);
		let axe_angle = atan(dy / params.wing_width);
		let axe = mk_cylinder(axe_radius, params.wing_width, params.precision);
		// let mini_axe = mk_cylinder(0.1, params.wing_width, params.precision);
		// mini_axe = translate([-0.5, 0.75 * params.wing_c_max, -10], mini_axe);
		axe = translate([-0.5, ratio * params.wing_c_max, -10], axe);
		//axe = union(axe, mini_axe);
		axe = rotate(-axe_angle, [1, 0, 0], axe);
		//axe = translate([0, ratio * params.wing_c_max, 0], axe);
		axe = rotate(params.wing_angle, [0, -1, 0], axe);
		axe = rotate(params.wing_dihedron - 90, [0, -1, 0], axe);
		axe = translate([params.body_radius + params.wing_x, y_min + params.nose_length + params.wing_y, z_min + params.wing_z], axe);
		axe = color([0, 0, 0], axe);
		// Wing Left
		let naca_left = new Naca4(params.wing_naca, params.wing_c_max);
		let naca_right = new Naca4(params.wing_naca, params.wing_c_min);
		let wing_left = naca_wing(naca_left, naca_right, params.wing_width, params.wing_arrow, params.precision);
		wing_left = rotate(params.wing_angle, [0, -1, 0], wing_left);
		wing_left = rotate(params.wing_dihedron, [0, -1, 0], wing_left);
		wing_left = translate([params.body_radius + params.wing_x, y_min + params.nose_length + params.wing_y, z_min + params.wing_z], wing_left);
		wing_left = difference(wing_left, axe);
		// wing_left = union(wing_left, axe);
		wing_left = color([0, 0, 1], wing_left);
		// Wing Right
		// let wing_right = mirror([1, 0, 0], wing_left);
		// 
		result.push(wing_left);
	}

	// let solid = union(nose, body, wing_left, wing_right);

	// let solid = CSG.fromPolygons([right]);

	return result;
}